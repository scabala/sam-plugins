-- version 1

local sam = require("sam/v1")
local log = require("log")

function versions()
    return sam.gitHubReleases("GoogleContainerTools", "container-diff")

end

function download(version, dest)
    base_url = "https://github.com/GoogleContainerTools/container-diff/releases/download/v" .. version

    log.warn("Does not work for versions older than 0.16.0")

    log.info("Downloading checksum file")
    ch = sam.fetch(base_url .. "/container-diff-linux-amd64.sha256")

    log.info("Downloading release binary")
    bi = sam.fetch(base_url .. "/container-diff-linux-amd64")
    if not sam.checksum(bi, ch)
    then
        log.error("Checksum from file does match calculated checksum")
        return
    end

    log.info("Install binary")
    sam.copy(bi, dest .. "container-diff", "0700")

end

function install(src)
    sam.symlink(src .. "container-diff", os.getenv("HOME") .. "/.local/bin/container-diff")
end

function uninstall(src)
    sam.unlink(os.getenv("HOME") .. "/.local/bin/container-diff")
end
