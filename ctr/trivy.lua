-- version 1

local sam = require("sam/v1")
local log = require("log")

function versions()
    return sam.gitHubReleases("aquasecurity", "trivy")

end

function download(version, dest)
    base_url = "https://github.com/aquasecurity/trivy/releases/download/v" .. version

    ch = sam.fetch(base_url .. "/trivy_" .. version .. "_checksums.txt")

    log.info("Downloading release archive")
    ar = sam.fetch(base_url .. "/trivy_" .. version .. "_Linux-64bit.tar.gz")

    if not sam.checksum(ar, ch)
    then
        log.error("Checksum from file does match calculated checksum")
        return
    end

    log.info("Unpacking binary")
    sam.unpack(ar, "trivy", dest .. "trivy", "0700")

end

function install(src)
    sam.symlink(src .. "trivy", os.getenv("HOME") .. "/.local/bin/trivy")
end

function uninstall(src)
    sam.unlink(os.getenv("HOME") .. "/.local/bin/trivy")
end
