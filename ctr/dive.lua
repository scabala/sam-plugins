-- version 1

local sam = require("sam/v1")
local log = require("log")

function versions()
    return sam.gitHubReleases("wagoodman", "dive")

end

function download(version, dest)
    base_url = "https://github.com/wagoodman/dive/releases/download/v" .. version

    log.info("Downloading checksum file")
    ch = sam.fetch(base_url .. "/dive_" .. version .. "_checksums.txt")

    log.info("Downloading release archive")
    ar = sam.fetch(base_url .. "/dive_" .. version .. "_linux_amd64.tar.gz")
    if not sam.checksum(ar, ch)
    then
        log.error("Checksum from file does match calculated checksum")
        return
    end

    log.info("Unpacking binary")
    sam.unpack(ar, "dive", dest .. "dive", "0700")

end

function install(src)
    sam.symlink(src .. "dive", os.getenv("HOME") .. "/.local/bin/dive")
end

function uninstall(src)
    sam.unlink(os.getenv("HOME") .. "/.local/bin/dive")
end
