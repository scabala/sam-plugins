-- version 1

local sam = require("sam/v1")
local log = require("log")

-- WIP !!!

-- local pub_key = [[
-- -----BEGIN PUBLIC KEY-----
-- MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEZxAfzrQG1EbWyCI8LiSB7YgSFXoI
-- FNGTyQGKHFc6/H8TQumT9VLS78pUwtv3w7EfKoyFZoP32KrO7nzUy2q6Cw==
-- -----END PUBLIC KEY-----
-- ]]

function versions()
    a = sam.gitHubReleases("sigstore", "cosign")
    return a
end

function download(version, dest)
    base_url = "https://github.com/sigstore/cosign/releases/download/v" .. version

    log.warn("Does not work for versions older than 0.6.0")
    log.warn("Not checksing signature! (not implemented)")

    log.info("Downloading checksum file")
    ch = sam.fetch(base_url .. "/cosign_checksums.txt")

    log.info("Downloading release binary")
    bi = sam.fetch(base_url .. "/cosign-linux-amd64")
    if not sam.checksum(bi, ch)
    then
        log.error("Checksum from file does match calculated checksum")
        return
    end

    log.info("Install binary")
    sam.copy(bi, dest .. "cosign", "0700")
end

function download_060(version, dest)
    base_url = "https://github.com/sigstore/cosign/releases/download/v" .. version

    log.warn("Does not work for versions other than 0.6.0")
    log.warn("Not checksing signature! (not implemented)")

    log.info("Downloading checksum file")
    ch = sam.fetch(base_url .. "/cosign_checksums.txt")

    log.info("Downloading release binary")
    bi = sam.fetch(base_url .. "/cosign_linux_amd64_" .. version .. "_linux_amd64")
    if not sam.checksum(bi, ch)
    then
        log.error("Checksum from file does match calculated checksum")
        return
    end

    log.info("Install binary")
    sam.copy(bi, desti .. "cosign", "0700")
end

function download_050_and_older(version, dest)
    base_url = "https://github.com/sigstore/cosign/releases/download/v" .. version

    log.warn("Does not work for versions younger than 0.5.0")
    log.warn("Not checksing signature! (not implemented)")

    log.info("Downloading checksum file")
    chf = sam.fetch(base_url .. "/cosign-linux-amd64.sha256")
    local file = io.open(chf, "r")
    if not file
    then
        log.error("Could not open file with checksum for reading")
        return
    end
    local ch = file:read()
    file:close()

    log.info("Downloading release binary")
    bi = sam.fetch(base_url .. "/cosign-linux-amd64")
    if not sam.checksum(bi, ch)
    then
        log.error("Checksum from file does match calculated checksum")
        return
    end

    log.info("Install binary")
    sam.copy(bi, dest .. "cosign", "0700")
end

function install(src)
    sam.symlink(src .. "cosign", os.getenv("HOME") .. "/.local/bin/cosign")
end

function uninstall(src)
    sam.unlink(os.getenv("HOME") .. "/.local/bin/cosign")
end
