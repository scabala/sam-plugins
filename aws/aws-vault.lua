-- version 1

local sam = require("sam/v1")
local log = require("log")

function versions()
    return sam.gitHubReleases("99designs", "aws-vault")
end

function download(version, dest)
    local base_url = "https://github.com/99designs/aws-vault/releases/download/" .. version

    log.info("Downloading checksum file")
    local ch = sam.fetch(base_url .. "/SHA256SUMS")

    log.info("Downloading release binary")
    local bi = sam.fetch(base_url .. "/aws-vault-linux-amd64")

    if not sam.checksum(bi, ch)
    then
        log.error("Checksum from file does match calculated checksum")
        return
    end

    log.info("Install binary")
    sam.copy(bi, dest .. "aws-vault", "0700")
end

function install(src)
    sam.symlink(src .. "aws-vault", os.getenv("HOME") .. "/.local/bin/aws-vault")
end

function uninstall(src)
    sam.unlink(os.getenv("HOME") .. "/.local/bin/aws-vault")
end

