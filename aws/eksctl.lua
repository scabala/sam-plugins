-- version 1

local sam = require("sam/v1")
local log = require("log")

local zsh_completions = os.getenv("HOME") .. "/.local/share/zsh/site-functions/"
local bash_completions = os.getenv("HOME") .. "/.local/share/bash-completion/completions/"
local fish_completions = os.getenv("HOME") .. "/.config/fish/completions/"

function versions()
    return sam.gitHubReleases("weaveworks", "eksctl")

end

function download(version, dest)
    base_url = "https://github.com/weaveworks/eksctl/releases/download/" .. version

    log.warn("Does not work for versions younger than 0.63.0")

    log.info("Downloading checksum file")
    ch = sam.fetch(base_url .."/eksctl_checksums.txt")

    log.info("Downloading release archive")
    ar = sam.fetch(base_url .. "/eksctl_Linux_amd64.tar.gz")
    if not sam.checksum(ar, ch)
    then
        log.error("Checksum from file does match calculated checksum")
        return
    end

    log.info("Unpacking binary")
    sam.unpack(ar, "eksctl", dest .. "eksctl", "0700")

    os.execute(dest .. "eksctl completion bash > " .. dest .. "completion.bash")
    os.execute(dest .. "eksctl completion zsh > " .. dest .. "completion.zsh")
    os.execute(dest .. "eksctl completion fish > " .. dest .. "completion.fish")
end

function install(src)
    sam.symlink(src .. "eksctl", os.getenv("HOME") .. "/.local/bin/eksctl")

    if not os.rename(zsh_completions, zsh_completions)
    then
        sam.symlink(src .. "completion.zsh", zsh_completions .. "_eksctl")
    end

    if not os.rename(bash_completions, bash_completions)
    then
        sam.symlink(src .. "completion.bash", bash_completions .. "eksctl")
    end

    if not os.rename(fish_completions, fish_completions)
    then
        sam.symlink(src .. "completion.fish", fish_completions .. "eksctl.fish")
    end
end

function uninstall(src)
    sam.unlink(os.getenv("HOME") .. "/.local/bin/eksctl")

    if not os.rename(zsh_completions, zsh_completions)
    then
        sam.unlink(zsh_completions .. "_eksctl")
    end

    if not os.rename(bash_completions, bash_completions)
    then
        sam.unlink(bash_completions .. "eksctl")
    end

    if not os.rename(fish_completions, fish_completions)
    then
        sam.unlink(fish_completions .. "eksctl.fish")
    end
end

