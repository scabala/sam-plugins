-- version 1

local sam = require("sam/v1")
local log = require("log")

function versions()
    return sam.gitHubReleases("kubernetes-sigs", "aws-iam-authenticator")

end

function download(version, dest)
    base_url = "https://github.com/kubernetes-sigs/aws-iam-authenticator/releases/download/v" .. version

    log.warn("Does not work for versions older than 0.5.5")

    log.info("Downloading checksum file")
    ch = sam.fetch(base_url .. "/authenticator_" .. version .. "_checksums.txt")

    log.info("Downloading release binary")
    bi = sam.fetch(base_url .. "/aws-iam-authenticator_" .. version .. "_linux_amd64")
    if not sam.checksum(bi, ch)
    then
        log.error("Checksum from file does match calculated checksum")
        return
    end

    sam.copy(bi, dest .. "aws-iam-authenticator", "0700")

end

function install(src)
    sam.symlink(src .. "aws-iam-authenticator", os.getenv("HOME") .. "/.local/bin/aws-iam-authenticator")
end

function uninstall(src)
    sam.unlink(os.getenv("HOME") .. "/.local/bin/aws-iam-authenticator")
end

