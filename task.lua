-- version 1

local sam = require("sam/v1")
local log = require("log")

function versions()
    return sam.gitHubReleases("go-task", "task")

end

function download(version, dest)
    base_url = "https://github.com/go-task/task/releases/download/v" .. version

    log.info("Downloading checksum file")
    ch = sam.fetch(base_url .. "/task_checksums.txt")

    log.info("Downloading release archive")
    ar = sam.fetch(base_url .. "/task_linux_amd64.tar.gz")
    if not sam.checksum(ar, ch)
    then
        log.error("Checksum from file does match calculated checksum")
        return
    end

    log.info("Unpacking binary")
    sam.unpack(ar, "task", dest .. "task", "0700")

end

function install(src)
    sam.symlink(src .. "task", os.getenv("HOME") .. "/.local/bin/task")
end

function uninstall(src)
    sam.unlink(os.getenv("HOME") .. "/.local/bin/task")
end

