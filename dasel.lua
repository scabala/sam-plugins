-- version 1

local sam = require("sam/v1")
local log = require("log")

function versions()
    return sam.gitHubReleases("TomWright", "dasel")
end

function download(version, dest)
    base_url = "https://github.com/TomWright/dasel/releases/download/v" .. version

    log.info("Downloading release binary")
    bi = sam.fetch(base_url .. "/dasel_linux_amd64")

    log.info("Install binary")
    sam.copy(bi, dest .. "dasel", "0700")

end

function install(src)
    sam.symlink(src .. "dasel", os.getenv("HOME") .. "/.local/bin/dasel")
end

function uninstall(src)
    sam.unlink(os.getenv("HOME") .. "/.local/bin/dasel")
end
