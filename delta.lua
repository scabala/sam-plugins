-- version 1

local sam = require("sam/v1")
local log = require("log")

function versions()
    return sam.gitHubReleases("dandavison", "delta")
end

function download(version, dest)
    local base_url = "https://github.com/dandavison/delta/releases/download/" .. version

    log.info("Downloading release archive")
    local ar = sam.fetch(base_url .. "/delta-" .. version .. "-x86_64-unknown-linux-musl.tar.gz")

    log.info("Unpacking binary")
    sam.unpack(ar, "delta-" .. version .. "-x86_64-unknown-linux-musl/delta", dest .. "delta", "0700")
end

function install(src)
    sam.symlink(src .. "delta", os.getenv("HOME") .. "/.local/bin/delta")
end

function uninstall(src)
    sam.unlink(os.getenv("HOME") .. "/.local/bin/delta")
end

