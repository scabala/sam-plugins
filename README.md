# sam-plugins

My personal plugins for Sam: https://gitlab.com/scabala/sam

## todo:

* pueue
* gopass
* rusty-man - man page viewer
* websocat

* youtube search
* youtube screen cast
* cdktf (npm)
* ansible (python)
* bitwarden cli
* vault cli
* flux cli
* argocd cli
* vcluster cli
* fd
* exa
* cicada
* nushell
* xlpr
* yazi
