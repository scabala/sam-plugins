-- version 1

local sam = require("sam/v1")
local log = require("log")

local zsh_completions = os.getenv("HOME") .. "/.local/share/zsh/site-functions/"
local bash_completions = os.getenv("HOME") .. "/.local/share/bash-completion/completions/"
local fish_completions = os.getenv("HOME") .. "/.config/fish/completions/"

function versions()
    return sam.gitHubReleases("mikefarah", "yq")
end

function download(version, dest)
    base_url = "https://github.com/mikefarah/yq/releases/download/v" .. version

    log.info("Downloading release binary")
    bi = sam.fetch(base_url .. "/yq_linux_amd64")

    log.info("Install binary")
    sam.copy(bi, dest .. "yq", "0700")

    os.execute(dest .. "yq shell-completion bash > " .. dest .. "completion.bash")
    os.execute(dest .. "yq shell-completion zsh > " .. dest .. "completion.zsh")
    os.execute(dest .. "yq shell-completion fish > " .. dest .. "completion.fish")
end

function install(src)
    sam.symlink(src .. "yq", os.getenv("HOME") .. "/.local/bin/yq")

    if not os.rename(zsh_completions, zsh_completions)
    then
        sam.symlink(src .. "completion.zsh", zsh_completions .. "_yq")
    end

    if not os.rename(bash_completions, bash_completions)
    then
        sam.symlink(src .. "completion.bash", bash_completions .. "yq")
    end

    if not os.rename(fish_completions, fish_completions)
    then
        sam.symlink(src .. "completion.fish", fish_completions .. "yq.fish")
    end
end

function uninstall(src)
    sam.unlink(os.getenv("HOME") .. "/.local/bin/yq")

    if not os.rename(zsh_completions, zsh_completions)
    then
        sam.unlink(zsh_completions .. "_yq")
    end

    if not os.rename(bash_completions, bash_completions)
    then
        sam.unlink(bash_completions .. "yq")
    end

    if not os.rename(fish_completions, fish_completions)
    then
        sam.unlink(fish_completions .. "yq.fish")
    end
end

