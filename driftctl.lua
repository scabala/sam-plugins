-- version 1

local sam = require("sam/v1")
local log = require("log")

local pub_key = [[
-----BEGIN PGP PUBLIC KEY BLOCK-----

mDMEYBv2ABYJKwYBBAHaRw8BAQdAstkQggX4bNXmfdiy+Cn6XrQLk0GNx+s4hbvu
Oi6DBS60JENsb3Vkc2tpZmYgPHNlY3VyaXR5QGNsb3Vkc2tpZmYuY29tPoiQBBMW
CAA4FiEEJ3ZmAFp/AdSE9jdtrMd2p5yCTr0FAmAb9gACGwMFCwkIBwIGFQoJCAsC
BBYCAwECHgECF4AACgkQrMd2p5yCTr3CCgEA5kYdx5TMTHUJXwVs64QpQB5neN41
y7EEnD7zWoZUMxcBAOeZxVsR6VZQENhpBpFcSJDSHAK6KDdrBYc2FpRDXQ4G
=L0r6
-----END PGP PUBLIC KEY BLOCK-----
]]


function versions()
    return sam.gitHubReleases("cloudskiff", "driftctl")

end

function download(version, dest)
    base_url = "https://github.com/cloudskiff/driftctl/releases/download/v" .. version

    log.info("Downloading checksum signature file")
    sig = sam.fetch(base_url .. "/driftctl_SHA256SUMS.gpg")

    log.info("Downloading checksum file")
    ch = sam.fetch(base_url .. "/driftctl_SHA256SUMS")
    if not sam.verifySignaturePGP(ch, sig, pub_key)
    then
        log.error("Checksum file does not match signature")
        return
    end

    log.info("Downloading release binary")
    bi = sam.fetch(base_url .. "/driftctl_linux_amd64")
    if not sam.checksum(bi, ch)
    then
        log.error("Checksum from file does match calculated checksum")
        return
    end

    sam.copy(bi, dest .. "driftctl", "0700")
end

function install(src)
    sam.symlink(src .. "driftctl", os.getenv("HOME") .. "/.local/bin/driftctl")
end

function uninstall(src)
    sam.unlink(os.getenv("HOME") .. "/.local/bin/driftctl")
end

