-- version 1

local sam = require("sam/v1")
local log = require("log")

function versions()
    return sam.gitHubReleases("gohugoio", "hugo")
end

function download(version, dest)
    local base_url = "https://github.com/gohugoio/hugo/releases/download/v" .. version

    log.info("Downloading checksum file")
    local ch = sam.fetch(base_url .. "/hugo_" .. version .. "_checksums.txt")

    log.info("Downloading release archive")
    local ar = sam.fetch(base_url .. "/hugo_" .. version .. "_Linux-64bit.tar.gz")
    if not sam.checksum(ar, ch)
    then
        log.error("Checksum from file does match calculated checksum")
        return
    end

    log.info("Unpacking binary")
    sam.unpack(ar, "hugo", dest .. "hugo", "0700")

end

function install(src)
    sam.symlink(src .. "hugo", os.getenv("HOME") .. "/.local/bin/hugo")
end

function uninstall(src)
    sam.unlink(os.getenv("HOME") .. "/.local/bin/hugo")
end

