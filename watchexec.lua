-- version 1

local sam = require("sam/v1")
local log = require("log")

local pub_key = [[
untrusted comment: minisign public key: 595C0E790D9AC9D3
RWTTyZoNeQ5cWcHG3r9eeUw7Ec83iuvBM4X5NLVYzF/mP6ZCNvmpHZ3R
]]

function versions()
    local vs = sam.gitHubReleases("watchexec", "watchexec", {"pre", "lib"})
    local ret = {}
    for i, v in ipairs(vs)
    do
        ret[i] = string.gsub(v, "cli%-v", "")
    end
    return ret
end

function download(version, dest)
    base_url = "https://github.com/watchexec/watchexec/releases/download/cli-v" .. version
    
    log.info("Downloading checksum signature file")
    sig = sam.fetch(base_url .. "/SHA512SUMS.auto.minisig")

    log.info("Downloading checksum file")
    ch = sam.fetch(base_url .. "/SHA512SUMS")
    if not sam.verifySignatureMinisign(ch, sig, pub_key)
    then
        log.error("Checksum file does not match signature")
        return
    end

    log.info("Downloading release archive")
    ar = sam.fetch(base_url .. "/watchexec-" .. version .. "-x86_64-unknown-linux-gnu.tar.xz")
    if not sam.checksum(ar, ch, "sha512")
    then
        log.error("Checksum frm file does match calculated checksum")
        return
    end

    log.info("Unpacking binary")
    sam.unpack(ar, "watchexec-" .. version .. "-x86_64-unknown-linux-gnu/watchexec", dest .. "watchexec", "0700")
end

function install(src)
    sam.symlink(src .. "watchexec", os.getenv("HOME") .. "/.local/bin/watchexec")
end

function uninstall(src)
    sam.unlink(os.getenv("HOME") .. "/.local/bin/watchexec")
end

