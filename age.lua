-- version 1

local sam = require("sam/v1")
local log = require("log")

function versions()
    return sam.gitHubReleases("FiloSottile", "age")
end

function download(version, dest)
    local base_url = "https://github.com/FiloSottile/age/releases/download/v" .. version

    log.info("Downloading release archive")
    local ar = sam.fetch(base_url .. "/age-v" .. version .. "-linux-amd64.tar.gz")

    log.info("Unpacking binary")
    sam.unpack(ar, "age/age", dest .. "age", "0700")
    sam.unpack(ar, "age/age-keygen", dest .. "age-keygen", "0700")

end

function install(src)
    sam.symlink(src .. "age", os.getenv("HOME") .. "/.local/bin/age")
    sam.symlink(src .. "age-keygen", os.getenv("HOME") .. "/.local/bin/age-keygen")
end

function uninstall(src)
    sam.unlink(os.getenv("HOME") .. "/.local/bin/age")
    sam.unlink(os.getenv("HOME") .. "/.local/bin/age-keygen")
end
