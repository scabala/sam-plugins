-- version 1

local sam = require("sam/v1")
local log = require("log")

function versions()
    return sam.gitHubReleases("tfsec", "tfsec")

end

function download(version, dest)
    base_url = "https://github.com/tfsec/tfsec/releases/download/v" .. version

    log.info("Downloading release binary")
    bi = sam.fetch(base_url .. "/tfsec-linux-amd64")

    log.info("Installing binary")
    sam.copy(bi, dest .. "tfsec", "0700")

end

function install(src)
    sam.symlink(src .. "tfsec", os.getenv("HOME") .. "/.local/bin/tfsec")
end

function uninstall(src)
    sam.unlink(os.getenv("HOME") .. "/.local/bin/tfsec")
end
