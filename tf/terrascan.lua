-- version 1

local sam = require("sam/v1")
local log = require("log")

function versions()
    return sam.gitHubReleases("tenable", "terrascan")
end

function download(version, dest)
    local base_url = "https://github.com/tenable/terrascan/releases/download/v" .. version

    log.info("Downloading checksum file")
    local ch = sam.fetch(base_url .. "/checksums.txt")

    log.info("Downloading release archive")

    local ar = sam.fetch(base_url .. "/terrascan_" .. version .. "_Linux_x86_64.tar.gz")
    if not sam.checksum(ar, ch)
    then
        log.error("Checksum from file does match calculated checksum")
        return
    end

    log.info("Unpacking binary")
    sam.unpack(ar, "terrascan", dest .. "terrascan", "0700")
end

function install(src)
    sam.symlink(src .. "terrascan", os.getenv("HOME") .. "/.local/bin/terrascan")
end

function uninstall(src)
    sam.unlink(os.getenv("HOME") .. "/.local/bin/terrascan")
end

