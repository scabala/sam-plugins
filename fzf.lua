-- version 1

local sam = require("sam/v1")
local log = require("log")

function versions()
    return sam.gitHubReleases("junegunn", "fzf")
end

function download(version, dest)
    local base_url = "https://github.com/junegunn/fzf/releases/download/" .. version

    log.info("Downloading checksum file")
    local ch = sam.fetch(base_url .. "/fzf_" .. version .. "_checksums.txt")

    log.info("Downloading release archive")
    local ar = sam.fetch(base_url .. "/fzf-" .. version .. "-linux_amd64.tar.gz")
    if not sam.checksum(ar, ch)
    then
        log.error("Checksum from file does match calculated checksum")
        return
    end

    log.info("Unpacking binary")
    sam.unpack(ar, "fzf", dest .. "fzf", "0700")
end

function install(src)
    sam.symlink(src .. "fzf", os.getenv("HOME") .. "/.local/bin/fzf")
end

function uninstall(src)
    sam.unlink(os.getenv("HOME") .. "/.local/bin/fzf")
end

