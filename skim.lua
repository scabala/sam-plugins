-- version 1

local sam = require("sam/v1")
local log = require("log")

function versions()
    return sam.gitHubReleases("lotabout", "skim")
end

function download(version, dest)
    local base_url = "https://github.com/lotabout/skim/releases/download/v" .. version

    log.info("Downloading release archive")
    local ar = sam.fetch(base_url .. "/skim-v" .. version .. "-x86_64-unknown-linux-musl.tar.gz")

    log.info("Unpacking binary")
    sam.unpack(ar, "sk", dest .. "sk", "0700")
end

function install(src)
    sam.symlink(src .. "sk", os.getenv("HOME") .. "/.local/bin/sk")
end

function uninstall(src)
    sam.unlink(os.getenv("HOME") .. "/.local/bin/sk")
end

