-- version 1

local sam = require("sam/v1")
local log = require("log")

function versions()
    return sam.gitHubReleases("str4d", "rage")
end

function download(version, dest)
    local base_url = "https://github.com/str4d/rage/releases/download/v" .. version

    log.info("Downloading release archive")
    local ar = sam.fetch(base_url .. "/rage-v" .. version .. "-x86_64-linux.tar.gz")

    log.info("Unpacking binary")
    sam.unpack(ar, "rage/rage", dest .. "rage", "0700")
    sam.unpack(ar, "rage/rage-keygen", dest .. "rage-keygen", "0700")
    sam.unpack(ar, "rage/rage-mount", dest .. "rage-dwimountkeygen", "0700")
end

function install(src)
    sam.symlink(src .. "rage", os.getenv("HOME") .. "/.local/bin/rage")
    sam.symlink(src .. "rage-keygen", os.getenv("HOME") .. "/.local/bin/rage-keygen")
    sam.symlink(src .. "rage-keygen", os.getenv("HOME") .. "/.local/bin/rage-mount")
end

function uninstall(src)
    sam.unlink(os.getenv("HOME") .. "/.local/bin/rage")
    sam.unlink(os.getenv("HOME") .. "/.local/bin/rage-keygen")
    sam.unlink(os.getenv("HOME") .. "/.local/bin/rage-mount")
end
