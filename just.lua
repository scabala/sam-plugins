-- version 1

local sam = require("sam/v1")
local log = require("log")

function versions()
    return sam.gitHubReleases("casey", "just")
end

function download(version, dest)
    base_url = "https://github.com/casey/just/releases/download/" .. version

    log.info("Downloading release archive")
    ar = sam.fetch(base_url .. "/just-" .. version .. "-x86_64-unknown-linux-musl.tar.gz")

    log.info("Unpacking binary")
    sam.unpack(ar, "just", dest .. "just", "0700")
end

function install(src)
    sam.symlink(src .. "just", os.getenv("HOME") .. "/.local/bin/just")
end

function uninstall(src)
    sam.unlink(os.getenv("HOME") .. "/.local/bin/just")
end
