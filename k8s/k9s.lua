-- version 1

local sam = require("sam/v1")
local log = require("log")

function versions()
    return sam.gitHubReleases("derailed", "k9s")
end

function download(version, dest)
    local base_url = "https://github.com/derailed/k9s/releases/download/v" .. version

    log.info("Downloading checksum file")
    local ch = sam.fetch(base_url .. "/checksums.txt")

    log.info("Downloading release archive")
    local ar = sam.fetch(base_url .. "/k9s_Linux_x86_64.tar.gz")
    if not sam.checksum(ar, ch)
    then
        log.error("Checksum from file does match calculated checksum")
        return
    end

    log.info("Unpacking binary")
    sam.unpack(ar, "k9s", dest .. "k9s", "0700")

end

function install(src)
    sam.symlink(src .. "k9s", os.getenv("HOME") .. "/.local/bin/k9s")
end

function uninstall(src)
    sam.unlink(os.getenv("HOME") .. "/.local/bin/k9s")
end
