-- version 1

local sam = require("sam/v1")
local log = require("log")

function versions()
    return sam.gitHubReleases("c-bata", "kube-prompt")

end

function download(version, dest)
    base_url = "https://github.com/c-bata/kube-prompt/releases/download/v" .. version

    log.info("Downloading release archive")
    ar = sam.fetch(base_url .. "/kube-prompt_v" .. version .. "_linux_amd64.zip")

    log.info("Unpacking binary")
    sam.unpack(ar, "kube-prompt", dest .. "kube-prompt", "0700")

end

function install(src)
    sam.symlink(src .. "kube-prompt", os.getenv("HOME") .. "/.local/bin/kube-prompt")
end

function uninstall(src)
    sam.unlink(os.getenv("HOME") .. "/.local/bin/kube-prompt")
end
