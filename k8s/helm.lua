-- version 1

local sam = require("sam/v1")
local log = require("log")

local zsh_completions = os.getenv("HOME") .. "/.local/share/zsh/site-functions/"
local bash_completions = os.getenv("HOME") .. "/.local/share/bash-completion/completions/"
local fish_completions = os.getenv("HOME") .. "/.config/fish/completions/"

function versions()
    return sam.gitHubReleases("helm", "helm")

end

function download(version, dest)
    base_url = "https://get.helm.sh"

    log.info("Downloading checksum file")
    local chf = sam.fetch(base_url .. "/helm-v" .. version .. "-linux-amd64.tar.gz.sha256")
    local file = io.open(chf, "r")
    if not file
    then
        log.error("Could not open file with checksum for reading")
        return
    end
    local ch = file:read()
    file:close()

    log.info("Downloading release archive")
    ar = sam.fetch(base_url .. "/helm-v" .. version .. "-linux-amd64.tar.gz")
    if not sam.checksumString(ar, ch)
    then
        log.error("Checksum from file does match calculated checksum")
        return
    end

    log.info("Unpacking binary")
    sam.unpack(ar, "linux-amd64/helm", dest .. "helm", "0700")

    os.execute(dest .. "helm completion bash > " .. dest .. "completion.bash")
    os.execute(dest .. "helm completion zsh > " .. dest .. "completion.zsh")
    os.execute(dest .. "helm completion fish > " .. dest .. "completion.fish")
end

function install(src)
    sam.symlink(src .. "helm", os.getenv("HOME") .. "/.local/bin/helm")

    if not os.rename(zsh_completions, zsh_completions)
    then
        sam.symlink(src .. "completion.zsh", zsh_completions .. "_helm")
    end

    if not os.rename(bash_completions, bash_completions)
    then
        sam.symlink(src .. "completion.bash", bash_completions .. "helm")
    end

    if not os.rename(fish_completions, fish_completions)
    then
        sam.symlink(src .. "completion.fish", fish_completions .. "helm.fish")
    end
end

function uninstall(src)
    sam.unlink(os.getenv("HOME") .. "/.local/bin/helm")

    if not os.rename(zsh_completions, zsh_completions)
    then
        sam.unlink(zsh_completions .. "_helm")
    end

    if not os.rename(bash_completions, bash_completions)
    then
        sam.unlink(bash_completions .. "helm")
    end

    if not os.rename(fish_completions, fish_completions)
    then
        sam.unlink(fish_completions .. "helm.fish")
    end
end
