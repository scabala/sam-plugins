-- version 1

local sam = require("sam/v1")
local log = require("log")

local zsh_completions = os.getenv("HOME") .. "/.local/share/zsh/site-functions/"
local bash_completions = os.getenv("HOME") .. "/.local/share/bash-completion/completions/"

function versions()
    return sam.gitHubReleases("kubernetes", "kubernetes")

end

function download(version, dest)
    base_url = "https://dl.k8s.io/release/v" .. version .. "/bin/linux/amd64"

    log.info("Downloading checksum file")
    local chf = sam.fetch(base_url .. "/kubectl.sha512")
    local file = io.open(chf, "r")
    if not file
    then
        log.error("Could not open file with checksum for reading")
        return
    end
    local ch = file:read()
    file:close()

    log.info("Downloading release binary")
    bi = sam.fetch(base_url .. "/kubectl")

    if not sam.checksumString(bi, ch, "sha512")
    then
        log.error("Checksum from file does match calculated checksum")
        return
    end

    log.info("Install binary")
    sam.copy(bi, dest .. "kubectl", "0700")

    os.execute(dest .. "kubectl completion bash > " .. dest .. "completion.bash")
    os.execute(dest .. "kubectl completion zsh > " .. dest .. "completion.zsh")
end

function install(src)
    sam.symlink(src .. "kubectl", os.getenv("HOME") .. "/.local/bin/kubectl")

    if not os.rename(zsh_completions, zsh_completions)
    then
        sam.symlink(src .. "completion.zsh", zsh_completions .. "_kubectl")
    end

    if not os.rename(bash_completions, bash_completions)
    then
        sam.symlink(src .. "completion.bash", bash_completions .. "kubectl")
    end
end

function uninstall(src)
    sam.unlink(os.getenv("HOME") .. "/.local/bin/kubectl")

    if not os.rename(zsh_completions, zsh_completions)
    then
        sam.unlink(zsh_completions .. "_kubectl")
    end

    if not os.rename(bash_completions, bash_completions)
    then
        sam.unlink(bash_completions .. "kubectl")
    end
end
