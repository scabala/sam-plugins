-- version 1

local sam = require("sam/v1")
local log = require("log")

local zsh_completions = os.getenv("HOME") .. "/.local/share/zsh/site-functions/"
local bash_completions = os.getenv("HOME") .. "/.local/share/bash-completion/completions/"
local fish_completions = os.getenv("HOME") .. "/.config/fish/completions/"

local zsh_rcd = os.getenv("HOME") .. "/.config/zsh/rc.d/"
local bash_rcd = os.getenv("HOME") .. "/.config/bash/rc.d/"
local fish_rcd = os.getenv("HOME") .. "/.config/fish/config.d/"

function versions()
    return sam.gitHubReleases("starship", "starship")

end

function download(version, dest)
    local base_url = "https://github.com/starship/starship/releases/download/v" .. version

    log.info("Downloading checksum file")
    local chf = sam.fetch(base_url .. "/starship-x86_64-unknown-linux-gnu.tar.gz.sha256")
    local file = io.open(chf, "r")
    if not file
    then
        log.error("Could not open file ", chf)
        return
    end
    local ch = file:read()
    file:close()

    log.info("Downloading release archive")
    local ar = sam.fetch(base_url .. "/starship-x86_64-unknown-linux-gnu.tar.gz")
    if not sam.checksumString(ar, ch)
    then
        log.error("Checksum from file does match calculated checksum")
        return
    end

    log.info("Unpacking binary")
    sam.unpack(ar, "starship", dest .. "starship", "0700")

    os.execute(dest .. "starship completions bash > " .. dest .. "completion.bash")
    os.execute(dest .. "starship completions zsh > " .. dest .. "completion.zsh")
    os.execute(dest .. "starship completions fish > " .. dest .. "completion.fish")

    file = io.open(dest .. "bash.rc", "w")
    file:write("eval \"$(starship init bash)\"")
    file:close()

    file = io.open(dest .. "zsh.rc", "w")
    file:write("eval \"$(starship init zsh)\"")
    file:close()

    file = io.open(dest .. "fish.config", "w")
    file:write("starship init fish | source")
    file:close()
end

function install(src)
    sam.symlink(src .. "starship", os.getenv("HOME") .. "/.local/bin/starship")

    if not os.rename(zsh_completions, zsh_completions)
    then
        sam.symlink(src .. "completion.zsh", zsh_completions .. "_starship")
    end

    if not os.rename(bash_completions, bash_completions)
    then
        sam.symlink(src .. "completion.bash", bash_completions .. "starship")
    end

    if not os.rename(fish_completions, fish_completions)
    then
        sam.symlink(src .. "completion.fish", fish_completions .. "starship.fish")
    end

    if not os.rename(bash_rcd, bash_rcd)
    then
        sam.symlink(src .. "bash.rc", bash_rcd .. "starship.sh")
    end

    if not os.rename(zsh_rcd, zsh_rcd)
    then
        sam.symlink(src .. "zsh.rc", zsh_rcd .. "starship.sh")
    end

    if not os.rename(fish_rcd, fish_rcd)
    then
        sam.symlink(src .. "fish.config", fish_rcd .. "starship.sh")
    end
end

function uninstall(src)
    sam.unlink(os.getenv("HOME") .. "/.local/bin/starship")

    if not os.rename(zsh_completions, zsh_completions)
    then
        sam.unlink(zsh_completions .. "_starship")
    end

    if not os.rename(bash_completions, bash_completions)
    then
        sam.unlink(bash_completions .. "starship")
    end

    if not os.rename(fish_completions, fish_completions)
    then
        sam.unlink(fish_completions .. "starship.fish")
    end

    if not os.rename(bash_rcd, bash_rcd)
    then
        sam.unlink(bash_rcd .. "starship.sh")
    end

    if not os.rename(zsh_rcd, zsh_rcd)
    then
        sam.unlink(zsh_rcd .. "starship.sh")
    end

    if not os.rename(fish_rcd, fish_rcd)
    then
        sam.unlink(fish_rcd .. "starship.sh")
    end
end

