-- version 1

local sam = require("sam/v1")
local log = require("log")

function versions()
    return sam.gitHubReleases("FiloSottile", "mkcert")

end

function download(version, dest)
    base_url = "https://github.com/FiloSottile/mkcert/releases/download/v" .. version

    log.info("Downloading release binary")
    bi = sam.fetch(base_url .. "/mkcert-v" .. version .. "-linux-amd64")

    log.info("Install binary")
    sam.copy(bi, dest .. "mkcert", "0700")

end

function install(src)
    sam.symlink(src .. "mkcert", os.getenv("HOME") .. "/.local/bin/mkcert")
end

function uninstall(src)
    sam.unlink(os.getenv("HOME") .. "/.local/bin/mkcert")
end

