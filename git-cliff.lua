-- version 1

local sam = require("sam/v1")
local log = require("log")

local pub_key = [[
-----BEGIN PGP PUBLIC KEY BLOCK-----

mQINBGDGYnUBEACkR2hvoECdJfof8YWnJdFh4uXeIce6rYwihTiEY6rnkHgV6b3R
Un2K7po5xwh5nz0/6XB+xFPszO7Q2wh8/RwP4AQH8Ce3wn5UeYvRw5qWXWxCRozU
FbskdtAF5tArN5JwTWihCQNDhPr7sI4rg07j/InWgq6+8PQ3YLjbI9IgKDtcwrTG
y8c47dqHlY45AyaQQEfmzQ9xeNWf4w/3U74WHozP0JjPhzayMygLfFg4dE/wLC+S
fVhdp+o4gR60CW8OITshUZzoY8sphy8meTs3uSCyja6xZtiw9WGinxBzcWtvME3J
awcQYbARz/2XRQAnsdpnCUFI3JmTR6inarAe9Ixfo1E39QpEAD/T7o/WpBP4ELZK
fsFmXHFzK2nevliDjAPSX8uEu1S6XzIUwA7lXFR2jRTTiRwLl6PKnRqsUl5qU+rC
ErRN5xKAddDkk1MkE/w8VmiUE6F8CdgY/QnksBsYgaZSfYBc0tKShTuEI/UE6GXf
+hd0lCYIFD4GbYWuG+tsLM9HxS8KsVZukqG4j2ZEACM68f5CkagfQm81vgm9XSAv
9Kf6kRwO/+KNbvIf9JCZYwLN6+jeBo1Ujmi/fBFZYY03LlWxOuy4/8Ku8oURw20Q
SnqSDxNIKY6HL7VEP8Ljp/zD5oczwZLq0fRStKtb6kb87vL8HDJ9pCmplwARAQAB
tCRnaXQtY2xpZmYgPGdpdC1jbGlmZkBwcm90b25tYWlsLmNvbT6JAk4EEwEIADgW
IQQdLUEKdBE368VEgm9KkvoXtmGSlwUCYMZidQIbAwULCQgHAgYVCgkICwIEFgID
AQIeAQIXgAAKCRBKkvoXtmGSl468EACZuFjYij3cA4CXU0vADKsEmse+PeqdCrKB
/sjZL/8vIIEvD6wEQCVGl/B4OwWQqHwcIXdsLfnIcEAfvvcM1hKZ+3at2TTDYEaN
Ekm9LNwayqTj5RGIAqmloBQqkxmEnDaAKnmiP2QkCFIelnUapX2L5UmP7Ver8I9/
x2q8Sz7lwDUtezXWi+3WfoDijO05oVNlULchAK/RYLEtxt0fVSdE8xKlu5dA3Wy/
EHZkevBLZFa0VNPKwsEcfkPSaODkZUGFxLkFQ5JCjIasvbHKbSeDFlwomhJ71+au
plaTlxli+RlT9GYV4P3wk7ZeyCwjs6Erb/Aa8/Urwm+kUw42QY+UD4a1YveJkFOK
Fnnm0hZ6snw28AZH9WSxrpDZnsM25czqEZMbm57FAC78yNMJ89DJffvAQVO8yk7R
hWbA4ik20pwQi2AVOxDaXb6cKi8G0I0eQk7BREk+JXLyZaUUcXj1CVp+l+6W3Ray
l5ftVMaW1PbdFaoXu+HVA84Mx7A4k+sbK/tt3hSNtVgQGkpU/K4y1UMFXYfjnm3W
zyuMet+1nrMvArFLApoPMGJFH/NsmASAU6kJVm0To/eAuRUWy9TpRkjIATXbSTi2
lrazz+42YWBpmAelgyRpdlM83NGB3Q1oOVVV28b367a5UhmBZuEM+uD5EUqKpbUU
V9V2DyNu0bkCDQRgxmJ1ARAA2yH770v6qWXEBR0k/hoqsRiwGHiJ5nNeLhm8TkC2
IPVyGlQj90OGA9XdUH5ZZGecJ9dyoMGIONl/dR6rfry2dY10neSpvPGYLBdJIUBm
1gLGy1tOUTG4BaBm7cOMoRMLwOhXt/DoXxohSSS+SMyMoodd7l2xtELZYuLgB3vr
T7+6A6K8HJ+5NF9ZnXD23S0AhzNASlp1D0K3tMBcJob/Sts1WOHlMtw+BLzpfXMn
6XStq5H/DcLpZfiPRDTm+czCSg7udzhh2vUHNEttph77dCIic8NpFvHFuIn4jqIo
9H9Yg3cUNSUPG+eaLSHRLKMq/ncDiqDg4erc7B6fcopJjnYmx6WzYGTzAvaEA0QW
4pwy9SzojXHIaojNQobcwsl0YPhvpCIQBhL1Y1U+3pRr/8wpmCA6loLnBpUYPtkH
9UIFSi05JEAhv/9IKH4dEVajpcAVJ8p7E1DBzswXGSxXf42xvtQ1znCNwq16PXPd
ejO+mZcWmfnvgsk3pJuLpHNwQ8KV2RAWXhxTMKf57elvNk/rLa1Rp+38AbYHPmK2
LwNPuMTgbAZnyX5UBPDCDoLBL+RKWMq5GGkgCyAE0AMcI8XLXlEYG0GCUkI/2mtU
O4WLuD0UuJYswdZOqZcnnUIF4JNyFaoCvHXXXS4qlyfDRVdeW27UL6EhFLvEKfUE
Z50AEQEAAYkCNgQYAQgAIBYhBB0tQQp0ETfrxUSCb0qS+he2YZKXBQJgxmJ1AhsM
AAoJEEqS+he2YZKXeh0P/1d0avbLaxS4JCuytyMDd3m5hWayx3ZJl2ik6/J2aLLg
1Tn33iRhUtDvh6iqtDYgc1WVDx1NOGUY2AoyXN4VxMnjksoIa014p3prRB09xDls
9nxE1rwgPv1DHVNQSSi+tBklOrvnohX5kZm3llM9dcpUwy1APOBkL1f5fGzJenze
IjmTGd38Ix2IE4YxGDE0+1BF1ed+AWJY9n8kvVuDzlW9982dtWbDvkiO+v/tTzRV
NVhP57V7y8QafZewMyF3sHwRvxN8qkM6LqletxFtc7gQ58D2Qma1g0SAx0HBLksO
A/b81BHS6IFboToAqqgUsU6L7P/9n32gMTQo64+AYeV4Wn/KPuu+WPdUrRPsEYAo
kmEJtA7FTo5NzHxWJVyRcePyzYE40bTM1KRApnHidVW2sdcYnDd/0YpzvacAME2b
56zoMgUM8mv7ViRV1J8xSTX4xk1v8LiU0hYJqo8RbgKA/IeBWcCoz8GKeYlF8ZAH
oEl/2HmGN/fu1k7uM8Ml2z5zSBtUHhqd1Yly6bqG5Sl/HlQcVkTT7zo3wABTwgqK
4EA65IrfOTV2FCWrTO8YgnCdv1BZfbRP7tl3m63Fins1xuQhXhkGbdn5RHftO22v
RCK6Nsms3RWCwWk9wsUlnht1jpbZWIesDTGSCvyuf03RW12LPOx5JfY/lThLZulP
=LcR7
-----END PGP PUBLIC KEY BLOCK-----
]]

function versions()
    return sam.gitHubReleases("orhun", "git-cliff")
end

function download(version, dest)
    base_url = "https://github.com/orhun/git-cliff/releases/download/v" .. version --0.6.0/git-cliff-0.6.0-x86_64-unknown-linux-gnu.tar.gz.sig"

    log.info("Downloading checksum file")
    local ch = sam.fetch(base_url .. "/git-cliff-" .. version .. "-x86_64-unknown-linux-gnu.tar.gz.sha512")

    log.info("Downloading release archive signature")
    local sig = sam.fetch(base_url .. "/git-cliff-" .. version .. "-x86_64-unknown-linux-gnu.tar.gz.sig")

    log.info("Downloading release archive")
    local ar = sam.fetch(base_url .. "/git-cliff-" .. version .. "-x86_64-unknown-linux-gnu.tar.gz")

    if not sam.checksum(ar, ch, "sha512")
    then
        error("Checksum from file does match calculated checksum")
    end

    if not sam.verifySignaturePGP(ar, sig, pub_key)
    then
        error("Archive file does not match signature")
    end

    log.info("Unpack binary")
    sam.unpack(ar, "git-cliff-" .. version .. "/git-cliff", dest .. "git-cliff", "0700")
end

function install(src)
    sam.symlink(src .. "git-cliff", os.getenv("HOME") .. "/.local/bin/git-cliff")
end

function uninstall(src)
    sam.unlink(os.getenv("HOME") .. "/.local/bin/git-cliff")
end

