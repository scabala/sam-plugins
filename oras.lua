-- version 1

local sam = require("sam/v1")
local log = require("log")

local pub_key = [[
-----BEGIN PGP PUBLIC KEY BLOCK-----

mQINBGK1bKABEAC9PJNr+gbFo8SE85A/GPWDBYVtdFhld+2hzupeuAeXHlK6yskR
gnw4bbM6N5SU1mMKVXY8avZDlqe6a0QNq+RQVfdE2Y75aaVp9VeueHVwGdG9BE+N
nOJn3zcfRJmx0oRSFjf30ijNcV7YWCxkJOOlS0JOmioQHLlCHRuPX/ozlhjaxL0e
CieQ0EuCqM7lyTmAE5SMwePThOkIRxO4oin1ogde/4th7OQIJrX5rsiPbGtXBWaq
LMM34aMIFr2ZQArvbJsBKhXDg7j50Pi3D0sNcE3AVaXoa5e43hj1PoO415eerHjc
Yu6VpgE/iXfxv6udkbDlPer3HZc1gJT1Mhu39pp5DpYgQBbUFGtnIQGkNJsPAViD
KUlJJrR9V1fQJv4ndqZhvYM2GnGE67H/DDrb9eonhP7A71ntq2me9VbSmD9kBHjf
RVd7GuqqA/RZpr+Ig5N6ipaaxWgrRu//fgE/kmrjU4HOXX8/2fEDvjsIbYBYkYVK
yMfoq9IgjRDGhA6TiB26yX2tk3H/baI0wcPnoIRTBFlZ19zBZazC1qUJM2aJXJos
tk92cDIvhQHW/0wvYztNqDNz6Mv4ADzxDELc9kX3VpK3+MPbiUy+iwe0Yvd3J9MY
hvfAmX6e7koxAH+Hg0r2BkluLf/kJhW34ZACtP2k28P15iWGqeTnq8g+JQARAQAB
tCJTaGl3ZWkgWmhhbmcgPHNoaXpoQG1pY3Jvc29mdC5jb20+iQJUBBMBCgA+FiEE
lbts1GyjDSucruVsnyzUN6l9y48FAmK1bKACGwMFCQHhM4AFCwkIBwIGFQoJCAsC
BBYCAwECHgECF4AACgkQnyzUN6l9y4/8xRAAtXPiipMFuX9sJRo8bHVULXBSZowk
9ReiIZtPIdlIzKweS434HUGlihYlFlmjNkp7v+12kovQE2xyw5rndBtrATsXZCyW
W5AtucMZJ5/YcIkBIRs52g3MqXUbxU/HU73WHuIBFC8q4YqFbS1VN+XDUvvwJ/wE
AnKBq4XuQzxsn05uIDQztdkrRP1f80lsDEbAD+aE3U7eMc4m8tXHcDFDbBwoI51+
eBkmusANLuH7y6wZnYBchaHAixTY5N9v0AG3OrDWmMzY3bqgw/wzL0Bj7e1M01+o
pKuVFtES5975KcOuaTR2UO/D1eJ962m4sdtdNgLMMlm4Ks3KGcL7S+A3ova/CypG
OhSiTYRx6rStRG6XSuMcWkBwAUn2BUdvigCynmkQQHoR9DcYfR9Ev/y1aNo4SEZq
lvMFatfFmGvKmBxuH/vA/duhLWx82byfXoNX1QbP31z2cCyOAVGxzHhQwJKANwG7
06kOQUIZ7JheEhU1lZ9Uqz3oyNSyCHGzmnG2PmGS0oI7qom9pGRk/TlVAqPtm/sM
iDIgCCY6b+egMBZR26dux3DYxd51ng75Mc5+zH1/uUY/AFFPiqfIjSxVdJbLnPNC
VzjUZynuXVc4RRWz8VXmzjQrPCoHmXhUlkZV0cpFbThRyRmALU8WAJgihNEr92bL
4zEshM5vd/lIZ8G5Ag0EYrVsoAEQAMbqLn+w/lFFUBNxeY2y8j8vCv2VqUNvQ6rN
TJ+Vy+psuCMHzGgAqQIDT9cUG/3htXI6dJK8ZYCxitMaTFTS3aFKKv9K4z0SKq7M
eaUdwplg4EMx2/G6GQ3F72X/baX/nYi7iLxp6zu8kDjR6ulBL/VuAg63ds6BrcIj
sEYoOT6wWAuw+d0nKtreAgflyD3KYPnOyJ4/VfOZEoSQfVY7LOQeVYakycSz8z2l
7RUOgUQhSFKPu4QadQZPpdJBje9kySgfnJnjJ5T9oTAC/rSyMVOCFqWwmtP3UM0c
rwspp46lmhlm4e8Wuo+DbJkiqUrcbFpEZxDYQI/JVtZyKJbXBYsd5UNIV+IWou93
4xfGqLSkZZysmEZ5Cm3c6ItNLQTMpJK0bX7ZIZSEj9ZMLZYfpm3/JL99ZfrCp2j7
0aDmtKWWRFmlfiUYEH+sxQUCv5msFwpM4Q6cWdWiQu+q2Odwd2tZ3NUzCiTnkwc9
SImV6PZZiyqtnUiTbHEOvOCXjlrpkUiYlJWKLNCFyMehGmhK+ndiDE5cZ8TbGivV
Q+wYw/DRUv8CjjBoZ/4jaoN9CJu6pZUe1xo5xhOM9FEznraWXDBqLabPicBxpLw3
YbSMg+MChG1vePBZMDIXNgDzS3/FKD75pRPk1mr6kDGPXsdLNeNH15VHpxJrfo6j
rNti06WzABEBAAGJAjwEGAEKACYWIQSVu2zUbKMNK5yu5WyfLNQ3qX3LjwUCYrVs
oAIbDAUJAeEzgAAKCRCfLNQ3qX3Lj3wsD/4yYXIDCzCFDUZSSuZcg9iprh29jD9w
2cuJ3MLOrwJ/pQxVlPWhj3xluiYftvzYzIwwxSg289SsIOgxQrFhDZabU+Obx8xY
3e7/teXB45X7E1xMLQYrAaLA0IVlFeqQCwqJKKl8izY+iJkfCCO8yWnfqbIoHocL
aGMgtkpXe34l58+dQuVXiTDHGN21XBfZ6lxPDiyovFxW04z6/eqfyLWblxH0e+7X
ZMQD4NTSsaKPOSgz4BycdwbzyR2+NKELmjl8Rb+rZv4qqLNo2CkeozDzZg16ijil
3SLxAGrmGi+5CG5ximSVFbfgEYKqM7E/4TBOtaGn6U8IlBad1PCUzyjAxO1bPA8y
p2iA1oU2fy9rbAMBFrgotbtUQVp5t/bFKtNg6C3HrR3bJuZQNQhN+YNxc9QlOGfS
as72IiX8qa5b/D2/MgsIq07kUsybNN9nkJstDQ+yMTyqXxc6Fsk1eqwh0yMAA6ET
gQWbOHFEDq4dlueUI1ydH1TyH0cs4BiIy+6RUpLt4HE5WifwI7UMaDgcj9UmXNXv
LY/Ld8rJXW/eMrQisy8A0jQq4k+RCnNJKisWz9mF7LX56uVIhMXQtpaG3l4kJirH
WR+r0WqPbwJ8WsE+RWI05oFX805jcYbNXhvke1QaUYb+G7rP4UE+hfKo2YZQQ3DL
yFueD7eURGTVAw==
=h9hw
-----END PGP PUBLIC KEY BLOCK-----
]]

local zsh_completions = os.getenv("HOME") .. "/.local/share/zsh/site-functions/"
local bash_completions = os.getenv("HOME") .. "/.local/share/bash-completion/completions/"
local fish_completions = os.getenv("HOME") .. "/.config/fish/completions/"

function versions()
    return sam.gitHubReleases("oras-project", "oras")
end

function download(version, dest)
    local base_url = "https://github.com/oras-project/oras/releases/download/v" .. version

    log.info("Downloading checksum signature file")
    sig = sam.fetch(base_url .. "/oras_" .. version .. "_checksums.txt.asc")

    log.info("Downloading checksum file")
    ch = sam.fetch(base_url .. "/oras_" .. version .. "_checksums.txt")
    if not sam.verifySignaturePGP(ch, sig, pub_key, true)
    then
        log.error("Checksum file does not match signature")
        return
    end

    log.info("Downloading release archive signature file")
    ar_sig = sam.fetch(base_url .. "/oras_" .. version .. "_linux_amd64.tar.gz.asc")

    log.info("Downloading release archive")
    ar = sam.fetch(base_url .. "/oras_" .. version .. "_linux_amd64.tar.gz")
    if not sam.verifySignaturePGP(ar, ar_sig, pub_key, true)
    then
        log.error("Checksum file does not match signature")
        return
    end

    if not sam.checksum(ar, ch)
    then
        log.error("Checksum from file does match calculated checksum")
        return
    end

    log.info("Unpacking binary")
    sam.unpack(ar, "oras", dest .. "oras", "0700")

    os.execute(dest .. "oras completion bash > " .. dest .. "completion.bash")
    os.execute(dest .. "oras completion zsh > " .. dest .. "completion.zsh")
    os.execute(dest .. "oras completion fish > " .. dest .. "completion.fish")
end

function install(src)
    sam.symlink(src .. "oras", os.getenv("HOME") .. "/.local/bin/oras")

    if not os.rename(zsh_completions, zsh_completions)
    then
        sam.symlink(src .. "completion.zsh", zsh_completions .. "_oras")
    end

    if not os.rename(bash_completions, bash_completions)
    then
        sam.symlink(src .. "completion.bash", bash_completions .. "oras")
    end

    if not os.rename(fish_completions, fish_completions)
    then
        sam.symlink(src .. "completion.fish", fish_completions .. "oras.fish")
    end
end

function uninstall(src)
    sam.unlink(os.getenv("HOME") .. "/.local/bin/oras")

    if not os.rename(zsh_completions, zsh_completions)
    then
        sam.unlink(zsh_completions .. "_oras")
    end

    if not os.rename(bash_completions, bash_completions)
    then
        sam.unlink(bash_completions .. "oras")
    end

    if not os.rename(fish_completions, fish_completions)
    then
        sam.unlink(fish_completions .. "oras.fish")
    end
end
