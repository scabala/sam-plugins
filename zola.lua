-- version 1

local sam = require("sam/v1")
local log = require("log")

function versions()
    return sam.gitHubReleases("getzola", "zola")
end

function download(version, dest)
    local base_url = "https://github.com/getzola/zola/releases/download/v" .. version

    log.info("Downloading release archive")
    local ar = sam.fetch(base_url .. "/zola-v" .. version .. "-x86_64-unknown-linux-gnu.tar.gz")

    log.info("Unpacking binary")
    sam.unpack(ar, "zola", dest .. "zola", "0700")

end

function install(src)
    sam.symlink(src .. "zola", os.getenv("HOME") .. "/.local/bin/zola")
end

function uninstall(src)
    sam.unlink(os.getenv("HOME") .. "/.local/bin/zola")
end

