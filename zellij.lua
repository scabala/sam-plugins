-- version 1

local sam = require("sam/v1")
local log = require("log")

local zsh_completions = os.getenv("HOME") .. "/.local/share/zsh/site-functions/"
local bash_completions = os.getenv("HOME") .. "/.local/share/bash-completion/completions/"
local fish_completions = os.getenv("HOME") .. "/.config/fish/completions/"

function versions()
    return sam.gitHubReleases("zellij-org", "zellij")
end

function download(version, dest)
    base_url = "https://github.com/zellij-org/zellij/releases/download/v" .. version

    log.info("Downloading release archive")
    ar = sam.fetch(base_url .. "/zellij-x86_64-unknown-linux-musl.tar.gz")

    log.info("Unpacking binary")
    sam.unpack(ar, "zellij", dest .. "zellij", "0700")

    os.execute(dest .. "zellij setup --generate-completion bash > " .. dest .. "completion.bash")
    os.execute(dest .. "zellij setup --generate-completion zsh > " .. dest .. "completion.zsh")
    os.execute(dest .. "zellij setup --generate-completion fish > " .. dest .. "completion.fish")
end

function install(src)
    sam.symlink(src .. "zellij", os.getenv("HOME") .. "/.local/bin/zellij")

    if not os.rename(zsh_completions, zsh_completions)
    then
        sam.symlink(src .. "completion.zsh", zsh_completions .. "_zellij")
    end

    if not os.rename(bash_completions, bash_completions)
    then
        sam.symlink(src .. "completion.bash", bash_completions .. "zellij")
    end

    if not os.rename(fish_completions, fish_completions)
    then
        sam.symlink(src .. "completion.fish", fish_completions .. "zellij.fish")
    end
end

function uninstall(src)
    sam.unlink(os.getenv("HOME") .. "/.local/bin/zellij")

    if not os.rename(zsh_completions, zsh_completions)
    then
        sam.unlink(zsh_completions .. "_zellij")
    end

    if not os.rename(bash_completions, bash_completions)
    then
        sam.unlink(bash_completions .. "zellij")
    end

    if not os.rename(fish_completions, fish_completions)
    then
        sam.unlink(fish_completions .. "zellij.fish")
    end
end

