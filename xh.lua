-- version 1

local sam = require("sam/v1")
local log = require("log")

local zsh_completions = os.getenv("HOME") .. "/.local/share/zsh/site-functions/"
local bash_completions = os.getenv("HOME") .. "/.local/share/bash-completion/completions/"
local fish_completions = os.getenv("HOME") .. "/.config/fish/completions/"

function versions()
    return sam.gitHubReleases("ducaale", "xh")
end

function download(version, dest)
    local base_url = "https://github.com/ducaale/xh/releases/download/v" .. version

    log.info("Downloading release archive")
    local ar = sam.fetch(base_url .. "/xh-v" .. version .. "-x86_64-unknown-linux-musl.tar.gz")

    log.info("Unpacking binary")
    sam.unpack(ar, "xh-v" .. version .. "-x86_64-unknown-linux-musl/xh", dest .. "xh", "0700")

    sam.unpack(ar, "xh-v" .. version .. "-x86_64-unknown-linux-musl/completions/xh.bash", dest .. "completion.bash", "0600")
    sam.unpack(ar, "xh-v" .. version .. "-x86_64-unknown-linux-musl/completions/xh.fish", dest .. "completion.fish", "0600")
    sam.unpack(ar, "xh-v" .. version .. "-x86_64-unknown-linux-musl/completions/_xh", dest .. "completion.zsh", "0600")
end

function install(src)
    sam.symlink(src .. "xh", os.getenv("HOME") .. "/.local/bin/xh")

    if not os.rename(zsh_completions, zsh_completions)
    then
        sam.symlink(src .. "completion.zsh", zsh_completions .. "_xh")
    end

    if not os.rename(bash_completions, bash_completions)
    then
        sam.symlink(src .. "completion.bash", bash_completions .. "xh")
    end

    if not os.rename(fish_completions, fish_completions)
    then
        sam.symlink(src .. "completion.fish", fish_completions .. "xh.fish")
    end
end

function uninstall(src)
    sam.unlink(os.getenv("HOME") .. "/.local/bin/xh")

    if not os.rename(zsh_completions, zsh_completions)
    then
        sam.unlink(zsh_completions .. "_xh")
    end

    if not os.rename(bash_completions, bash_completions)
    then
        sam.unlink(bash_completions .. "xh")
    end

    if not os.rename(fish_completions, fish_completions)
    then
        sam.unlink(fish_completions .. "xh.fish")
    end
end

