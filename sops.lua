-- version 1

local sam = require("sam/v1")
local log = require("log")

function versions()
    return sam.gitHubReleases("mozilla", "sops")

end

function download(version, dest)
    base_url = "https://github.com/mozilla/sops/releases/download/v" .. version

    log.warn("Some older versions does not work")

    log.info("Downloading release binary")
    bi = sam.fetch(base_url .. "/sops-v" .. version .. ".linux")

    log.info("Install binary")
    sam.copy(bi, dest .. "sops", "0700")

end

function install(src)
    sam.symlink(src .. "sops", os.getenv("HOME") .. "/.local/bin/sops")
end

function uninstall(src)
    sam.unlink(os.getenv("HOME") .. "/.local/bin/sops")
end

