-- version 1

local sam = require("sam/v1")
local log = require("log")

local zsh_completions = os.getenv("HOME") .. "/.local/share/zsh/site-functions/"
local bash_completions = os.getenv("HOME") .. "/.local/share/bash-completion/completions/"
local fish_completions = os.getenv("HOME") .. "/.config/fish/completions/"

function versions()
    return sam.gitHubReleases("cli", "cli")

end

function download(version, dest)
    base_url = "https://github.com/cli/cli/releases/download/v" .. version

    log.info("Downloading checksum file")
    ch = sam.fetch(base_url .. "/gh_" .. version .. "_checksums.txt")

    log.info("Downloading release archive")
    ar = sam.fetch(base_url .. "/gh_" .. version .. "_linux_amd64.tar.gz")
    if not sam.checksum(ar, ch)
    then
        log.error("Checksum from file does match calculated checksum")
        return
    end

    log.info("Unpacking binary")
    sam.unpack(ar, "gh_" ..version .. "_linux_amd64/bin/gh", dest .. "gh", "0700")

    os.execute(dest .. "gh completion --shell bash > " .. dest .. "completion.bash")
    os.execute(dest .. "gh completion --shell zsh > " .. dest .. "completion.zsh")
    os.execute(dest .. "gh completion --shell fish > " .. dest .. "completion.fish")
end

function install(src)
    sam.symlink(src .. "gh", os.getenv("HOME") .. "/.local/bin/gh")

    if not os.rename(zsh_completions, zsh_completions)
    then
        sam.symlink(src .. "completion.zsh", zsh_completions .. "_gh")
    end

    if not os.rename(bash_completions, bash_completions)
    then
        sam.symlink(src .. "completion.bash", bash_completions .. "gh")
    end

    if not os.rename(fish_completions, fish_completions)
    then
        sam.symlink(src .. "completion.fish", fish_completions .. "gh.fish")
    end
end

function uninstall(src)
    sam.unlink(os.getenv("HOME") .. "/.local/bin/gh")

    if not os.rename(zsh_completions, zsh_completions)
    then
        sam.unlink(zsh_completions .. "_gh")
    end

    if not os.rename(bash_completions, bash_completions)
    then
        sam.unlink(bash_completions .. "gh")
    end

    if not os.rename(fish_completions, fish_completions)
    then
        sam.unlink(fish_completions .. "gh.fish")
    end
end
