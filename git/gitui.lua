-- version 1

local sam = require("sam/v1")
local log = require("log")

function versions()
    return sam.gitHubReleases("extrawurst", "gitui")
end

function download(version, dest)
    local base_url = "https://github.com/extrawurst/gitui/releases/download/v" .. version

    log.info("Downloading release archive")
    local ar = sam.fetch(base_url .. "/gitui-linux-musl.tar.gz")

    log.info("Unpacking binary")
    sam.unpack(ar, "gitui", dest .. "gitui", "0700")
end

function install(src)
    sam.symlink(src .. "gitui", os.getenv("HOME") .. "/.local/bin/gitui")
end

function uninstall(src)
    sam.unlink(os.getenv("HOME") .. "/.local/bin/gitui")
end

